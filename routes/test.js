var express = require('express');
var router = express.Router();
var connector = require('../lib/connector')

router.get('/', function(req, res, next){
    connector.select(function(err, data){
        if(err){
            console.log("ERROR : "+err);
        }
        res.render('test', { data : data });
    });
});

router.post('/add', function(req, res, next){
    connector.insert(req.body.name, function(err, data){
        if(err){
            res.status(500).send("DB에 추가 실패 Error message : "+err);
        }
        console.log("DB Insert success");
        res.redirect('.');
    });
});

router.post('/delete', function(req, res, next){
    connector.del(req.body.id, function(err, data){
        if(err){
            res.status(500).send("DB에 삭제 실패 Error message : "+err);
        }
        console.log("DB Delete success ");
        res.redirect('.');
    });
});

router.post('/update', function(req, res, next){
    connector.update(req.body.id, req.body.name, function(err, data){
        if(err){
            res.status(500).send("DB에 수정 실패 Error message : "+err);
        }
        console.log("DB Delete success");
        res.redirect('.');
    });
});

module.exports = router;
