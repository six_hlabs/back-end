'use strict';

var ageSymbol = Symbol()
class class_exam{
	constructor(name){
		this.name = name
		this[ageSymbol] = 0
	}

	get age(){
		return this[ageSymbol]
	}
	set age(value){
		this[ageSymbol] = value
	}

	doSomething(){
		console.log("I'am a " + this.name + " Age is "+this[ageSymbol])
	}
}

var ex = new class_exam('jun')
ex.age = 2 
ex.doSomething()
