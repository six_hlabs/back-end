var db = require('./_db_pool');

function select(callback){
    db.getConnection(function(err, conn){
        conn.query("select * from hlabs_test", function (err, rows) {
            if (err) {
                conn.release();
                callback(err, null);
            }
            conn.release();
            callback(null, rows);
        });
    });
}

function insert(name, callback){
    db.getConnection(function(err, conn){
        conn.query("insert into hlabs_test(name) values('"+name+"');", function (err, rows) {
            if (err) {
                conn.release();
                callback(err, null);
            }
            conn.release();
            callback(null, rows);
        });
    });
}

function update(id, name, callback){
    db.getConnection(function(err, conn){
        conn.query("update hlabs_test set name = '"+name+"' where id = '+"+id+"';", function (err, rows) {
            if (err) {
                conn.release();
                callback(err, null);
            }
            conn.release();
            callback(null, rows);
        });
    });
}

function del(id, callback){
    db.getConnection(function(err, conn){
        conn.query("delete from hlabs_test where id = '"+id+"';", function (err, rows) {
            if (err) {
                conn.release();
                callback(err, null);
            }
            conn.release();
            callback(null, rows);
        });
    });
}

module.exports = {
    select : select,
    insert : insert,
    update : update,
    del : del,
};